import React from 'react';
import ReactDOM from 'react-dom/client';
//import './index.css';
import App from './App';
//import reportWebVitals from './reportWebVitals';

//Import Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));

  root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();

// const name = 'James Bond';

// let element = <h1> Hello, {name} </h1>

// /*  
//     JSX (JavaScript + XML)
//       - an extenstion of JavaScript that let's us create objects which will be then compiled and added as HTML elements
// */

// // Create a user object
//   const user = 
//   {
//     firstName: 'Jane',
//     lastName: 'Smith'
//   }

//   // function that will use the user as parameter
//   function formatName (profile)
//   {
//     return profile.firstName + " " + profile.lastName;
//   }

//   element = <h1>Hello, {formatName(user)}! </h1>
//     /*  
//         <h1> is an example of JSX
//         JSX allows us to create HTML elements and at the same time allows us to apply JavaScript code to these elements making it easy to write both HTML and JavaScript code in a single file as opposed to creating two separate files (One for HTML and another for JavaScript syntax).
//     */

// root.render(element);
// // allows to render/display our reactJS elements and display in our HTML
